Carrefour webapp
========
### Assets

[Demo presentation](https://slides.com/robbiebardijn-1/deck)
[Full Documentation](https://drive.google.com/file/d/1z8gmoOvtfrlVLv-CckCMi6mv9ONF3k4O/view?usp=sharing)
[How to add a module](https://drive.google.com/open?id=1EI-eHjUJfEn7sk7nL2J6_ofawgDHvWvA)

### General

The carrefour **Web App** is a demo project to prove that we can perform a wide range of actions based on the mobile app, for the web. [here](http://carrefour.endare.com/components/) you can find the demo project.

The demo project is **hooked into the uat environment** of Carrefour.
To log in you need a test account, you can use: *jeroen.piette@hotmail.com / Test1234*


### Installation

To install the project you must have a **node** and **npm** installation.

1. Install [node](https://nodejs.org/en/) *(Preferably the 'Recommended For most Users' Version.)*
2. Clone the repo with `git clone git@bitbucket.org:endarebvba/crfsa_web.git`
3. Go to the folder with the terminal and type `npm i`


### Running & building

* After installation you can run the project with browsersync `npm run demo`
* Alternatively you can run `npm run compile` if you just want to build.


### Documentation

##### Structure & files

    /Users/robbiebardijn/Documents/PROJECTS/crfsa_web_app/
    + build/
    + .git/
    + assets/
    - src/
      - js/
        app.js
        shellaroundapp.js
        helper.js

      - scss/
        style.scss

      .DS_Store

    .babelrc
    index.html
    .DS_Store
    ADBMobileConfig.json
    user.html
    tree.xiki
    webpack.config.js
    README.md
    .gitignore
    package.json
    manifest.json


##### ES2016+

Everything gets compiled trough **webpack** and **babeljs**.
The npm run demo also fires a watcher on the files to produce changes to the browser.

##### Initialisation

	init () {
	    document.addEventListener('crf_module_init',  (result) => {
	      var register = result.detail;

	      var moduleData = {
	        name: 'AddToBasketWebmodule', // Set the name of the module to be registered
	        version: '0.0.1', // Set the registration version of the module, all versions of all modules must match
	        eventTypes: [
	          'core/state/running',
	          'core/api/onToken',
	          'core/api/request',

	          'authentication/isAuthenticated',
	          'authentication/isAuthenticatedDone',

	          'store/singleProductByBiggestStore',
	          'store/singleProductByBiggestStoreDone',
	          'store/getDriveStore',
	          'store/getDefaultTimeslot',
	          'store/getDefaultTimeslotDone',
	          'analytics/trackView',
	          'store/searchIngredient'
	        ],
	        isInitialModule: false, // Only 1 module can be the Initial Module
	        isAuthenticationModule: false // Only 1 module can be the authenticationModule
	      };

	      // // Call registration function with the moduleData variable to register it
	      register(moduleData,  (eventbus) => {
	        this.eventBus = eventbus;
	        this.eventBus.registerHandler("core/state/running", (eventType) => {
	          this.registerEvents();
	        });
	      });
	    });
	  }

##### Events

See slideshow

##### Helpers

    export default class Helper {
      static showToaster (text) {
        var toaster = document.querySelector('.toaster');
        var toasterMessage = document.querySelector('.toaster__message');

        toaster.classList.add("toaster--shown");
        toasterMessage.innerHTML = text;

        let timer = setTimeout(() => {
          toaster.classList.remove("toaster--shown");
          clearTimeout(timer);
        }, 5000);
      };

      static checkDriveStore () {
        if (localStorage.getItem("store.drive") === null) { return false; }
        return true;
      }

      static loadJS (url, implementationCode, location){
        let scriptTag = document.createElement('script');
        scriptTag.src = url;
        scriptTag.onload = implementationCode;
        scriptTag.onreadystatechange = implementationCode;
        location.appendChild(scriptTag);
      };
    }


### Communication with the mobile app

In the mobile app we made a config difference between **WEB** and **APP**.
This is to make checks in the app if the webapp needs to do something different than the mobile app, for example go to a url in a webpage, or send an event.

When you go to branch `robbie/module-de-branching` in the **mobile app repo** you have a `ENV=uat TARGET=web npm run ship` script. This will **build the app** and **move the build files** in the right folder of the **web project folder**...
You can choose another name if you want this.
