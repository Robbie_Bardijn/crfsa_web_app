/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1327);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1327:
/***/ function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_env__ = __webpack_require__(136);

window.digitalData = {
    'environment': __WEBPACK_IMPORTED_MODULE_0__app_env__["a" /* env */].production ? 'PROD' : 'DEV',
    'siteType': 'CRFAPP',
    'events': [],
    'user': {}
};
function loadAdobeJsSdk() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = 'async';
    script.defer = 'defer';
    script.src = 'https://assets.adobedtm.com/91e09c727495016b8c4346df8dbeb32719d7e08e/satelliteLib-822f57fa55e8b5b0c9285ff1863078a75dac1dc3' + (__WEBPACK_IMPORTED_MODULE_0__app_env__["a" /* env */].production ? '.js' : '-staging.js');
    script.onload = function () {
        var satelliteScript = document.createElement('script');
        satelliteScript.type = 'text/javascript';
        satelliteScript.text = '_satellite.pageBottom();';
        if (window.ADB) {
            document.body.appendChild(satelliteScript);
        }
        if (window.onAdobeLoad) {
            window.onAdobeLoad();
        }
    };
    document.getElementsByTagName('head')[0].appendChild(script);
}
if (!window.cordova) {
    loadAdobeJsSdk();
}
else {
    document.addEventListener('deviceready', function () {
        var ADB = window.ADB;
        if (ADB) {
            ADB.visitorGetMarketingCloudId(function (marketingCloudId) {
                if (window.history && window.history.replaceState) {
                    var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?adobe_mc=' + marketingCloudId;
                    console.log(newUrl);
                    window.history.replaceState(null, '', newUrl);
                }
                setTimeout(function () {
                    // load JS SDK
                    loadAdobeJsSdk();
                }, 100);
            }, function (error) {
                console.log('error of get marketing cloud id', error);
            });
        }
    });
}
//# sourceMappingURL=adobe.js.map

/***/ },

/***/ 136:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return env; });
var env = {
    api: {
        url: 'https://int.api.carrefour.eu',
        key: 'y2sscu57tx4ttp8bjb4g9ewf',
        secret: 'rACw2dXKEN',
        fingerprint: '3A FA 48 B1 11 61 73 87 E1 0A 19 C5 03 CE 78 DF 17 FA FD EA'
    },
    cookie: {
        domain: '.carrefour.eu',
        authToken: 'CRFUSERTOKENQA'
    },
    wkApi: {
        url: 'http://51.137.104.114'
    },
    push: {
        id: 704920877189
    },
    maxSendCheckInEmployeeCalls: 4,
    production: false
};
//# sourceMappingURL=qa.js.map

/***/ }

/******/ });
//# sourceMappingURL=adobe.js.map