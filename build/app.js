/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _shellaroundapp__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shellaroundapp */ \"./src/js/shellaroundapp.js\");\n/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helper */ \"./src/js/helper.js\");\n// =========================== //\n// Imports //\n// =========================== //\n\n\n\n// All click events on .link and .button\nfunction clickButton(e) {\n\n  // console.log(typeof e.target);\n  localStorage.removeItem('carrefour.lastEvent');\n\n  if (shell.initialized) {\n    e.preventDefault();\n    shell.detectEvent(e.currentTarget.getAttribute('data-action'), e.currentTarget.getAttribute('data-action-params'));\n  } else {\n    var eventData = {action: e.currentTarget.getAttribute('data-action'), params: e.currentTarget.getAttribute('data-action-params'), fallback: e.currentTarget.getAttribute('data-action-fallback')}\n    localStorage.setItem('carrefour.lastEvent', JSON.stringify(eventData));\n\n    if(!e.currentTarget.hasAttribute('href')) {\n      window.location.href = e.currentTarget.getAttribute('data-action-fallback');\n    }\n  }\n}\n\n// Attach eventlisteners\nvar buttons = document.querySelectorAll('.button, .link');\nbuttons.forEach(button => {\n  button.addEventListener(\"click\", clickButton, true);\n})\n\n// Make an instance of shellaroundapp\nlet shell = new _shellaroundapp__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\nshell.init();\n_helper__WEBPACK_IMPORTED_MODULE_1__[\"default\"].loadJS('build/main.js', function(){}, document.body);\n\n// add classes to body to make the layout look ok\ndocument.body.classList.add(\"not-logged-in\");\ndocument.body.classList.add(\"no-store\");\n\n\n//# sourceURL=webpack:///./src/js/app.js?");

/***/ }),

/***/ "./src/js/helper.js":
/*!**************************!*\
  !*** ./src/js/helper.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Helper; });\nclass Helper {\n  static showToaster (text) {\n    var toaster = document.querySelector('.toaster');\n    var toasterMessage = document.querySelector('.toaster__message');\n\n    toaster.classList.add(\"toaster--shown\");\n    toasterMessage.innerHTML = text;\n\n    let timer = setTimeout(() => {\n      toaster.classList.remove(\"toaster--shown\");\n      clearTimeout(timer);\n    }, 5000);\n  };\n\n  static checkDriveStore () {\n    if (localStorage.getItem(\"store.drive\") === null) { return false; }\n    return true;\n  }\n\n  static loadJS (url, implementationCode, location){\n    let scriptTag = document.createElement('script');\n    scriptTag.src = url;\n    scriptTag.onload = implementationCode;\n    scriptTag.onreadystatechange = implementationCode;\n    location.appendChild(scriptTag);\n  };\n}\n\n\n//# sourceURL=webpack:///./src/js/helper.js?");

/***/ }),

/***/ "./src/js/shellaroundapp.js":
/*!**********************************!*\
  !*** ./src/js/shellaroundapp.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return ShellAroundApp; });\n/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helper */ \"./src/js/helper.js\");\n\n\nclass ShellAroundApp {\n  constructor() {\n    this.eventBus = null;\n    this.initialized = false;\n    this.state = \"\";\n  }\n\n  // Initiate the module and its\n  init () {\n    document.addEventListener('crf_module_init',  (result) => {\n      var register = result.detail;\n\n      var moduleData = {\n        name: 'AddToBasketWebmodule', // Set the name of the module to be registered\n        version: '0.0.1', // Set the registration version of the module, all versions of all modules must match\n        eventTypes: [\n          'core/state/running',\n          'core/api/onToken',\n          'core/api/request',\n\n          'authentication/isAuthenticated',\n          'authentication/isAuthenticatedDone',\n\n          'store/singleProductByBiggestStore',\n          'store/singleProductByBiggestStoreDone',\n          'store/getDriveStore',\n          'store/getDefaultTimeslot',\n          'store/getDefaultTimeslotDone',\n          'analytics/trackView',\n          'store/searchIngredient'\n        ],\n        isInitialModule: false, // Only 1 module can be the Initial Module\n        isAuthenticationModule: false // Only 1 module can be the authenticationModule\n      };\n\n      // Call registration function with the moduleData variable to register it\n      register(moduleData,  (eventbus) => {\n        this.eventBus = eventbus;\n        this.eventBus.registerHandler(\"core/state/running\", (eventType) => {\n          this.registerEvents();\n        });\n      });\n    });\n  }\n\n  // Method to register all initial events\n  registerEvents () {\n    this.eventBus.registerHandler(\"core/closeApp\", (eventType, basket) => {\n      if (document.body.classList.contains('app-open')) {\n        document.body.classList.remove('app-open');\n      }\n    });\n\n    this.eventBus.registerHandler(\"basket/getDone\", (eventType, basket) => {\n      document.body.classList.add(\"basket-known\");\n\n      if (basket) {\n        document.querySelector(\".items-in-basket\").innerHTML = basket.productCount;\n      }\n\n      if (!this.initialized) {\n        this.initialized = true;\n        if(localStorage.getItem('carrefour.lastEvent')) {\n          var lastEvent = JSON.parse(localStorage.getItem('carrefour.lastEvent'))\n          this.detectEvent(lastEvent.action, lastEvent.params);\n        }\n      }\n    });\n\n    this.eventBus.registerHandler(\"store/getDriveStore\", (eventType, store) => {\n      if(store) {\n        _helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].showToaster(\"Je voegde drive store toe.\")\n      }\n\n    });\n\n    this.eventBus.registerHandler(\"authentication/isAuthenticatedDone\", (eventType, isAuthenticated) => {\n      if (!isAuthenticated) {\n        _helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].showToaster(\"Je bent niet ingelogd. Klik eerst op het user icoon.\");\n      }\n      document.body.classList.add(\"basket-known\");\n\n    });\n\n    this.eventBus.registerHandler(\"core/stateTransition\", (eventType, data) => {\n      this.state = data;\n    });\n\n    this.eventBus.throwEvent(\"authentication/isAuthenticated\");\n  }\n\n  detectEvent (action, ...params) {\n\n    // =========================== //\n    // User clicked on show basket //\n    // =========================== //\n    if (action && action == \"show-basket\") {\n      if (!document.body.classList.contains('app-open')) {\n        document.body.classList.add('app-open');\n\n        if(!_helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].checkDriveStore()) {\n          if(this.state !== \"FavStorePage\") {\n            this.searchStore();\n          }\n        } else {\n          if(this.state !== \"BasketPage\") {\n            this.showBasket();\n          }\n        }\n\n      } else {\n        document.body.classList.remove('app-open');\n      }\n    }\n\n    // ============================ //\n    // User clicked on show product //\n    // ============================ //\n    if (action && action == \"show-productdetail\") {\n      if (!document.body.classList.contains('app-open')) {\n        document.body.classList.add('app-open');\n      }\n\n      if(!_helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].checkDriveStore()) {\n        if(this.state !== \"FavStorePage\") {\n          this.searchStore();\n        }\n      } else {\n        this.showProductDetail(params[0]);\n      }\n    }\n\n    // ===================================== //\n    // User clicked on add product to basket //\n    // ===================================== //\n    if (action && action == \"add-product-to-basket\") {\n      if(!_helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].checkDriveStore()) {\n        if (!document.body.classList.contains('app-open')) {\n          document.body.classList.add('app-open');\n        }\n        if(this.state !== \"FavStorePage\") {\n          this.searchStore();\n        }\n      } else {\n        if (document.body.classList.contains('app-open')) {\n          document.body.classList.remove('app-open');\n        }\n        this.addProductToBasket(params[0]);\n      }\n    }\n\n    // ================================= //\n    // User clicked on search ingredient //\n    // ================================= //\n    if (action && action == \"search-ingredient\") {\n      if (!document.body.classList.contains('app-open')) {\n        document.body.classList.add('app-open');\n      }\n\n\n      if(!_helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].checkDriveStore()) {\n        if(this.state !== \"FavStorePage\") {\n          this.searchStore();\n        }\n      } else {\n        this.searchIngredient(params[0]);\n      }\n    }\n\n    // ============================ //\n    // User clicked on select store //\n    // ============================ //\n    if (action && action == \"show-store\") {\n      if (!document.body.classList.contains('app-open')) {\n        document.body.classList.add('app-open');\n      }\n\n      if(this.state !== \"FavStorePage\") {\n        this.searchStore();\n      }\n    }\n  }\n\n  showBasket () {\n    console.log(\"SHOW BASKET\");\n    // Throw navigation event to basketPage\n    this.eventBus.throwEvent('app/navigate', {key: 'drive/basketPage'});\n  }\n\n  searchStore () {\n    console.log(\"SEARCH STORE\");\n    // Throw navigation event to favstorePage\n    this.eventBus.throwEvent('app/navigate', {key: 'store/favStore'});\n  }\n\n  showProductDetail (productId) {\n    console.log(\"SHOW PRODUCT DETAIL:\", productId);\n    const singleProductCallback = (eventType, product) => {\n      if (product) {\n        this.eventBus.throwEvent('app/navigate', {\n            key: 'drive/productInfoPage',\n            params: {\n              product: product\n            }\n        });\n      }\n      this.eventBus.unregisterHandlerByFunction(singleProductCallback);\n    }\n\n    const timeslotCallback = (eventType, time) => {\n      this.eventBus.throwEvent(\"store/singleProductByBiggestStore\", {product: productId});\n      this.eventBus.unregisterHandlerByFunction(timeslotCallback);\n    }\n\n    this.eventBus.registerHandler(\"store/singleProductByBiggestStoreDone\", singleProductCallback);\n    this.eventBus.registerHandler(\"store/getDefaultTimeslotDone\", timeslotCallback);\n\n    // Throw event to get the default timeslot, tis is mandatory to get a product\n    this.eventBus.throwEvent(\"store/getDefaultTimeslot\");\n  }\n\n  addProductToBasket (productId) {\n    console.log(\"ADD PRODUCT TO BASKET:\", productId);\n\n    const cbUpdate = (eventType, basket) => {\n      var myProduct = null;\n      basket.products.forEach((product) => {\n        if(product.ref == productId) {\n          myProduct = product;\n          product.addAmount(product.stepToAddOrRemove);\n          _helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].showToaster(\"Je voegde \" + product.shortDesc + \"(\" + product.price.stdPrice + \"€) toe.\")\n          this.eventBus.throwEvent(\"basket/update\", product);\n        }\n      });\n\n      if(myProduct) {\n        if(this.state !== \"BasketPage\"){\n          this.eventBus.throwEvent('app/navigate', {key: 'drive/basketPage'});\n        }\n      } else {\n        this.eventBus.registerHandler(\"store/singleProductByBiggestStoreDone\", singleProductCallback);\n        this.eventBus.registerHandler(\"store/getDefaultTimeslotDone\", timeslotCallback);\n\n        // Throw event to get the default timeslot, tis is mandatory to get a product\n        this.eventBus.throwEvent(\"store/getDefaultTimeslot\");\n      }\n\n      this.eventBus.unregisterHandlerByFunction(cbUpdate);\n    }\n\n    const cbUpdateSingle = (eventType, basket) => {\n      if(this.state !== \"BasketPage\"){\n        this.eventBus.throwEvent('app/navigate', {key: 'drive/basketPage'});\n      }\n      this.eventBus.unregisterHandlerByFunction(cbUpdateSingle);\n    }\n\n    const singleProductCallback = (eventType, product) => {\n      if (product) {\n        product.addAmount(product.stepToAddOrRemove);\n        _helper__WEBPACK_IMPORTED_MODULE_0__[\"default\"].showToaster(\"Je voegde \" + product.shortDesc+ \" (\" + product.price.stdPrice + \"€) toe.\")\n        this.eventBus.registerHandler(\"basket/getDone\", cbUpdateSingle);\n        this.eventBus.throwEvent(\"basket/update\", product);\n      }\n      this.eventBus.unregisterHandlerByFunction(singleProductCallback);\n    }\n\n    const timeslotCallback = (eventType, time) => {\n      this.eventBus.throwEvent(\"store/singleProductByBiggestStore\", {product: productId});\n      this.eventBus.unregisterHandlerByFunction(timeslotCallback);\n    }\n\n    this.eventBus.registerHandler(\"basket/getDone\", cbUpdate);\n    this.eventBus.throwEvent(\"basket/get\");\n  }\n\n  searchIngredient (ingredient) {\n    console.log(\"SEARCH INGREDIENT\", ingredient);\n    console.log(this.state);\n    // Check if you are on the categoriespage first...go to it when you are not on it to see the results.\n    if(this.state !== \"CategoriesPage\") {\n      this.eventBus.throwEvent('app/navigate', {key: 'drive'});\n    }\n\n    // throw custom event\n    this.eventBus.throwEvent(\"store/searchIngredient\", ingredient);\n  }\n}\n\n\n\n//# sourceURL=webpack:///./src/js/shellaroundapp.js?");

/***/ })

/******/ });