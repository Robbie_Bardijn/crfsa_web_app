var ctrl = angular.module('app.controllers', []);

ctrl.controller('StoreLocatorController', ['$scope', '$http', function($scope, $http) {
    var tries = 0,
        limit = 10;

    var _this = {
        onCreate: function() {
            _this.getGoogleMaps();

            window.postMessage = function(msg) {
                if(msg && msg.config) {
                    $scope.locator = msg.config;

                    _this.openMap();
                }
            };
        },

        getGoogleMaps() {
            //We do it this way, because IOS blocks this script when used in a script tag
            $http.get("http://maps.googleapis.com/maps/api/js?key=AIzaSyB82o8EAIBqyJ4eOm8IAMza7XNaqp1bK50&libraries=places")
                .success(function(data, code, headers, config) {
                    eval(data);
                }).error(function(data, code, headers, config) {
                    console.error('Error while trying to load the google maps sdk ', data);
                });
        },

        openMap: function() {
            var btn = document.getElementById('picker_delivery');

            //just to be sure that everything is loaded correctly
            if (!btn || typeof google == 'undefined') {
                tries++;

                if (tries >= limit) {
                    return;
                }

                return setTimeout(_this.openMap, 200);
            }

            setTimeout(function() {
                btn.click();
            }, 200);
        }
    };

    $scope.storeLocatorCallback = function(params) {
        console.log('---------------------------------------------');
        console.log(params);
        console.log('---------------------------------------------');
    };

    _this.onCreate();
}]);
