var lgStorePicker = angular.module('lg-store-picker',
	['style-sheet-factory','popup-directive','angular.css.injector']);

lgStorePicker.filter('unique', function() {
   return function(collection, keyname) {
	  var output = [],
		  keys = [];

	  angular.forEach(collection, function(item) {
		  var key = item[keyname];
		  if(keys.indexOf(key) === -1) {
			  keys.push(key);
			  output.push(item);
		  }
	  });
	  return output;
   };
});

lgStorePicker.run(['usefullFn', function(usefullFn) {
    usefullFn.loadContent();
}]);

lgStorePicker.service('restService', ['$http','$interval', function ($http, $interval) {
		var key = 'esppn85dgw8src3krf3b9d8h';
    	// var urlBase = 'http://dev.api.carrefour.eu/v1/storeLocator/';
		var urlBase = 'http://int.api.carrefour.eu/v1/storeLocator/';
		//var urlBase = 'http://api.be.carrefour.io/v1/storeLocator/';

        var getHash = function(){
				var currentDate = Math.round(new Date().getTime()/1000);
				var secret = 'QQW8Vvxycr';
				return Crypto.SHA256(key+secret+currentDate.valueOf());
		};
		var hash = getHash();
		var getHeader = function(){
    			var config = {headers: {
            		'Content-Type' : 'application/json',
            		'Accept' : 'application/json',
					'X-Signature': hash,
					'X-Api-Key': key
    			}};
    			return config;
    	};

    	var startCountdown = function() {
        	$interval(function() {
         		hash= getHash();
        	}, 300000);
     	};
     	startCountdown();

        this.getLocations = function (locationsParameters) {
            return $http.get(urlBase + 'locations' + locationsParameters, getHeader());
        };

        this.getCategories = function (categoriesParameters) {
            return $http.get(urlBase + 'serviceCategories' + categoriesParameters, getHeader());
        };

        this.getServices = function (servicesParameters) {
            return $http.get(urlBase + 'services' + servicesParameters, getHeader());
        };

        this.getLocation = function (id) {
        	return $http.get(urlBase + 'locations/' + id, getHeader());
        };
    }]);

lgStorePicker.service('usefullFn',function ($http, $interval) {
	var content = [];

	this.loadContent = function(){
        $http.get("lg-store-picker/content/contentText.json").then(function(response){
            content = response.data[0];
        },function(error) {
            console.log('Cannot load text content', error);
        },function(notify) {
            console.log('Notify', notify);
        });
	}

	this.getContent = function (language){
		return content[language.toLowerCase()];
	}

	this.getFormatedAddress = function(address, zipCode, language){
		if(!address){
			if(zipCode){
				if(zipCode.indexOf(content["fr"].belgium) != -1 || zipCode.indexOf(content["nl"].belgium) != -1){
					return zipcode;
				}else{
					return (zipCode + ", " + content[language.toLowerCase()].belgium);
				}
			}else{
				return "";
			}
		}else{
			if(address.indexOf(content["fr"].belgium) != -1 || address.indexOf(content["nl"].belgium) != -1){
				return address;
			}else{
				return (address + ", " + content[language.toLowerCase()].belgium);
			}
		}
	}
});

lgStorePicker.directive('lgStorePicker', ['styleSheetFactory','restService','usefullFn','$q', function(styleSheetFactory,restService,usefullFn,$q) {
	return {
		restrict: 'EA',
		scope: {
			id: '@',
			theme: '@',
			directory: '@',
			useBubbleLayout: '@',
			showBottomCards: '@',
			initVisibilityBottomCards: '@',
			allowGeoLoc: '@',
			isLad: '@',
			language: '@',
			zipcode: '@',
			address: '@',
			position: '@',
			locationType: '@',
			servicesFixed: '@',
			servicesVisible: '@',
			servicesChecked: '@',
			locationName: '@',
			locationId: '@',
			openDate: '@',
			callback: '@'
		},
		controller: function($scope, $http, cssInjector,$timeout,$q) {
			cssInjector.add($scope.directory + "/styles/style.css?v=17");
      		cssInjector.add($scope.directory + "/styles/"+$scope.theme+".css?v=13");
      		cssInjector.add($scope.directory + "/styles/main.css?v=14");

			$scope.n=30;
			$scope.showLocationPopup=false;
    		$scope.markers = [];
			$scope.slickLocations=[];
			$scope.loading=false;

			function getLocationsFromAPI(mapy, adaptZoom) {
				var locationsParameters = '?';

				if ($scope.lgParameters.lgLanguage != undefined) {
					locationsParameters += 'language=' + $scope.lgParameters.lgLanguage + '&';
				}

				if ($scope.lgParameters.lgPosition != undefined) {
					locationsParameters += 'latitude=' + $scope.lgParameters.lgPosition[0] + '&';
					locationsParameters += 'longitude=' + $scope.lgParameters.lgPosition[1] + '&';
					locationsParameters += 'radius=400&';
						// + $scope.lgParameters.lgPosition[2] + '&';
				}
				if ($scope.lgParameters.lgLocationType != undefined) {
					locationsParameters += 'locType=' + $scope.lgParameters.lgLocationType + '&';
				}

				if ($scope.lgParameters.lgOpenDate != undefined && $scope.lgParameters.lgOpenDate != "") {
					locationsParameters += 'openAt=date:' + $scope.lgParameters.lgOpenDate + '&';
				}

				if($scope.openSundaySubcategory){
					if(locationsParameters.indexOf('openAt=')== -1){
						locationsParameters += 'openAt=sunday&';
					}else{
						locationsParameters = locationsParameters.slice(0, -1);
						locationsParameters += ',sunday&';
					}
				}

				if($scope.openNowSubcategory){
					if(locationsParameters.indexOf('openAt=')== -1){
						locationsParameters += 'openAt=now&';
					}else{
						locationsParameters = locationsParameters.slice(0, -1);
						locationsParameters += ',now&';
					}
				}
				locationsParameters+='n='+$scope.n+'&';

				var activeServicesArray = [];

				for (eachService of $scope.servicesPerInstance[$scope.id]) {
					if (eachService.active == true) {
						activeServicesArray.push(eachService.serviceID);
					}
				}

				var activeServices = activeServicesArray.join(',');

				if (activeServices != undefined && activeServices != '') {
					locationsParameters += 'service=' + activeServices + '&';
				}

				//locationsParameters += 'ver=17';
				console.log(locationsParameters);

				restService.getLocations(locationsParameters).then(function(response){
				    $scope.locations = response.data;
				    if(mapy != undefined){
				  		for (var i = 0; i < $scope.markers.length; i++ ) {
				   		 	$scope.markers[i].setMap(null);
			  			}
			  			$scope.markers.length = 0;
						for (storeData of $scope.locations) {
							storeData.distance=storeData.distance.toFixed(1);
							for(singleService of storeData.serviceList){
								if(singleService.serviceID.indexOf('brand_')!= -1){
									storeData.brand = singleService.serviceID.substring(6);
									break;
								}
							}
							$scope.getPopupTemplate(mapy, storeData.locationID,
							storeData.name, (storeData.address.street + ' ' +storeData.address.streetNumber),
							(storeData.address.zipCode + ' ' + storeData.address.city), storeData.address.coordinates, storeData.address.phone, storeData.address.fax, storeData.url, storeData.locType, storeData.distance, storeData.brand);
						}
						if(adaptZoom){
							$scope.fitZoomLevel();
                        }
					}

					if($scope.showBottom){
						$scope.updateSlick();
					}
					if($scope.loading){
						$scope.loading=false;
					}
				}).catch(function(response) {
  					console.error('Could not retrieve locations', response.status, response.data);
				});
			}

			$scope.getMenuDataFromAPI = function () {
				var deferred = $.Deferred();

				var serviceCategoriesParameters = '?';
				if ($scope.lgParameters.lgLanguage != undefined) {
					serviceCategoriesParameters += 'language=' + $scope.lgParameters.lgLanguage;
				}

				var servicesParameters = '?';
				if ($scope.lgParameters.lgLanguage != undefined) {
					servicesParameters += 'language=' + $scope.lgParameters.lgLanguage;
				}

				if ($scope.servicesFixed == undefined) {
					$scope.servicesFixed = '';
				}
				if ($scope.servicesChecked == undefined) {
					$scope.servicesChecked = '';
				}
				if ($scope.servicesVisible == undefined) {
					$scope.servicesVisible = '';
				}

				$q.all({ categories: restService.getCategories(serviceCategoriesParameters), services: restService.getServices(servicesParameters)}).then(function(results) {

    				if ($scope.categoriesPerInstance == undefined) {
						$scope.categoriesPerInstance = [];
					}
					$scope.categoriesPerInstance[$scope.id] = results.categories.data;

					if ($scope.servicesPerInstance == undefined) {
						$scope.servicesPerInstance = [];
					}
					$scope.servicesPerInstance[$scope.id] = results.services.data;

					for (eachService of $scope.servicesPerInstance[$scope.id]) {
						var reg = new RegExp("\\b"+ eachService.serviceID +"\\b", "g");
						if(eachService.categoryList != undefined && eachService.categoryList.length > 0){
							eachService.category = eachService.categoryList[0];
						}
						if ($scope.servicesChecked.match(reg) || $scope.servicesFixed.match(reg)) {
							eachService.active = true;
						}
					}
					deferred.resolve();
  				});

  				return deferred.promise();
			}

			$scope.getDarkStores = function(zipCode){
				var darkStoreParameters = '?';
				if ($scope.lgParameters.lgLanguage != undefined) {
					darkStoreParameters += 'language=' + $scope.lgParameters.lgLanguage + '&';
				}else{
					darkStoreParameters += 'language=fr&';
				}
                if ($scope.lgParameters.lgLocationType != undefined) {
                    darkStoreParameters += 'locType=' + $scope.lgParameters.lgLocationType + '&';
                }else{
                    darkStoreParameters += 'locType=DarkStore&';
				}
				darkStoreParameters += 'zipcode=' + zipCode;

				restService.getLocations(darkStoreParameters).then(function(response){
					var darkStores = response.data;
					$scope.ladAvailable = true;
					$scope.darkStoreId = darkStores[0].locationID;
					//$scope.callUserFunction(darkStores[0].locationID);
				},function(error) {
		            console.log('No DarkStore found', error);
		            $scope.ladAvailable = false;
		        },function(notify) {
		            console.log('Notify', notify);
		        });
			}

			$scope.addAttributesToScope = function(theme,language,zipCode,address,
				position,locationType,servicesFixed,servicesVisible,servicesChecked,
				locationName,openDate,userPosition,callback,showBottomCards,initVisibilityBottomCards,allowGeoLoc, isLad, locationId) {
					if(isLad != undefined && isLad=='true'){
							$scope.ladActive=true;
					}else{
						$scope.ladActive=false;
					}
					if(language != undefined) {
						$scope.lgParameters.lgLanguage = language;
						$scope.content = usefullFn.getContent($scope.lgParameters.lgLanguage);
					}else{
						$scope.lgParameters.lgLanguage = "fr";
						$scope.content = usefullFn.getContent("fr");
					}
					if(zipCode != undefined) {
						$scope.lgParameters.lgZipcode = zipCode;
					}
					if(address != undefined) {
						$scope.lgParameters.lgAddress = address;
					}
					if(position) {
						position = position.replace(/\s+/g, '');
						$scope.lgParameters.lgPosition = position.split(',');
					}
					if(locationId  != undefined){
                        restService.getLocation(locationId).then(function(response){
							var location = response.data;
							if(location != undefined && location.address != undefined && location.address.coordinates != undefined){
                                if($scope.lgParameters.lgPosition == undefined){
                                    $scope.lgParameters.lgPosition = [];
								}
								$scope.lgParameters.lgPosition[0] = location.address.coordinates.latitude;
                                $scope.lgParameters.lgPosition[1] = location.address.coordinates.longitude;
                                $scope.showLocationPopup=true;
							}
                        },function(error) {
							console.log("Location with id " + locationId + " not found. ", error);
                        });
					}
					if(locationType != undefined && locationType != '') {
						$scope.lgParameters.lgLocationType = locationType;
					}
					if(servicesFixed != undefined) {
						servicesFixed = servicesFixed.replace(/\s+/g, '');
						$scope.lgParameters.lgServicesFixed = servicesFixed.split(',');
					}
					if(servicesVisible != undefined) {
						servicesVisible = servicesVisible.replace(/\s+/g, '');
						$scope.lgParameters.lgServicesVisible = servicesVisible.split(',');
					}
					if(servicesChecked != undefined) {
						servicesChecked = servicesChecked.replace(/\s+/g, '');
						$scope.lgParameters.lgServicesChecked = servicesChecked.split(',');
					}
					if(locationName != undefined) {
						$scope.lgParameters.lgLocationName = locationName;
					}
					if(openDate) {
						var dateArray = openDate.split(',');
						if(dateArray.indexOf("sunday") != -1 ){
							$scope.openSundaySubcategory=true;
						}
						if(dateArray.indexOf("now") != -1 ){
							$scope.openNowSubcategory=true;
						}
						var stringDate=openDate.match(/(\d{4})([\-])(\d{1,2})\2(\d{1,2})/);
						if(stringDate){
							stringDate = new Date(stringDate[0]);
							var month=stringDate.getMonth()+1;
							if(month<10){
								month="0"+month;
							}
							$scope.lgParameters.lgOpenDate =stringDate.getFullYear()+"-"+month+"-"+stringDate.getDate();
						}
					}
					if(userPosition != undefined) {
						$scope.lgParameters.lgUserPosition = userPosition;
					}
					if(callback != undefined) {
						$scope.lgParameters.lgCallback = callback;
					}
					if(showBottomCards != undefined) {
						if(showBottomCards=='false'){
							$scope.showBottom=false;
							//$('#bottom_content').remove();
						}else{
							$scope.showBottom=true;
						}
					}else{
						$scope.showBottom=true;
					}

					if(initVisibilityBottomCards != undefined){
						if(initVisibilityBottomCards=='false'){
							$scope.showContentBottom = false;
						}else{
							$scope.showContentBottom = true;
						}
					}else{
						$scope.showContentBottom = true;
					}

					if(allowGeoLoc != undefined){
						if(allowGeoLoc=='false' || $scope.ladActive){
							$scope.hideGeoloc=true;
							//$(".gm-geolocate").remove();
						}
					}else{
						$scope.hideGeoloc=true;
					}
					//getMenuDataFromAPI();
			};


			var infoWindow;


			$scope.getPopupTemplate = function(map, storeId, storeName, storeAdressFirstPart, storeAdressSecondPart, storeCoordinates, storePhone, storeFax, storeUrl, locType, distance, brand) {
				$http.get($scope.directory + '/infobubble/'+$scope.useBubbleLayout+'.html?ver=13').success(function(data) {
					var popupTemplate = data;
					popupTemplate = popupTemplate.replace(/scopeId/g,$scope.id);
					popupTemplate = popupTemplate.replace(/storeDataId/g,storeId);
					popupTemplate = popupTemplate.replace(/popupTheme/g,$scope.theme);
					popupTemplate = popupTemplate.replace(/storeDataName/g,storeName);
					popupTemplate = popupTemplate.replace(/storeLocationType/g,locType);
					popupTemplate = popupTemplate.replace(/storeDistance/g,distance);
					popupTemplate = popupTemplate.replace(/storeDataAdress1/g,storeAdressFirstPart);
					popupTemplate = popupTemplate.replace(/storeDataAdress2/g,storeAdressSecondPart);
					popupTemplate = popupTemplate.replace(/storeDataPhone/g,storePhone);
					popupTemplate = popupTemplate.replace(/storeDataFax/g,storeFax);
					popupTemplate = popupTemplate.replace(/storeDataUrl/g,storeUrl);
                    popupTemplate = popupTemplate.replace(/route/g,$scope.content.route);
                    popupTemplate = popupTemplate.replace(/chooseMarket/g,$scope.content.chooseMarketButton);

					setMarker(map, new google.maps.LatLng(storeCoordinates.latitude, storeCoordinates.longitude), storeName, popupTemplate, brand);
					if($scope.showLocationPopup){
						map.setCenter(new google.maps.LatLng($scope.locations[0].address.coordinates.latitude, $scope.locations[0].address.coordinates.longitude));
        				google.maps.event.trigger($scope.markers[0],'click');
       					$scope.showLocationPopup=false;
					}
				});

			};

			$scope.getResults = function(mapy,adaptZoom) {
				getLocationsFromAPI(mapy,adaptZoom);
			};

			$scope.openNowSubcategory = false;
			$scope.openSundaySubcategory = false;

			$scope.chooseButtonText = 'Choisir magasin';
			$scope.chooseButtonVisibility = 'show';
			if (window.currentCategory == undefined) {
				window.currentCategory = [];
			}
			window.currentCategory[$scope.id] = '';
			//console.log(window.currentCategory);
			$scope.currentCategory = '';
			$scope.lgParameters = [];

			if (window.lgStorePickerScope == undefined) {
				window.lgStorePickerScope = [];
			}

			window.lgStorePickerScope[$scope.id] = $scope;

			//console.log(window);

			// place a marker
			function setMarker(map, position, title, content, brand) {
					var marker;
					var markerOptions = {
							position: position,
							map: map,
							title: title,
							icon: $scope.directory + '/img/'+ brand +'.png'
					};

					marker = new google.maps.Marker(markerOptions);
					$scope.markers.push(marker);
					google.maps.event.addListener(marker, 'click', function () {
							// close window if not undefined
							if (infoWindow !== void 0) {
									infoWindow.close();
							}
							// create new window
							var infoWindowOptions = {
									content: content
							};
							infoWindow = new google.maps.InfoWindow(infoWindowOptions);
							infoWindow.open(map, marker);
					});
					 // add marker to array
			}
		},
		templateUrl: function(scope, elem, attr) {
			var templateLink = elem.directory + '/templates/template.html';
			return templateLink;
		},
		link: function(scope, element, attrs, ctrl) {
			// var styleSheet = styleSheetFactory.getStyleSheet();
			//
			// styleSheetFactory.addCSSRule(styleSheet, '#map',
			// 	'height: 1px;'
			// );

			scope.updateView = function(value) {
				ctrl.$viewValue = value;
				ctrl.$render();
				console.log(ctrl);
			};



			scope.addAttributesToScope(attrs.theme,attrs.language,attrs.zipcode,attrs.address,
				attrs.position,attrs.locationType,attrs.servicesFixed,attrs.servicesVisible,attrs.servicesChecked,
				attrs.locationName,attrs.openDate,attrs.userPosition,attrs.callback,attrs.showBottomCards,attrs.initVisibilityBottomCards,attrs.allowGeoLoc,attrs.isLad,attrs.locationId);


			scope.updateModel = function(id, name) {
					ctrl.$modelValue = id;
					scope.chooseButtonText = name;
					//window[scope.lgCallback](id);
			};

			scope.callUserFunction = function(value) {
				var nameOfFunction = scope.lgParameters.lgCallback;
				/*restService.getLocation(value).then(function(response){
				    console.log(response.data);
				}).catch(function(response) {
  					console.error('Could not retrieve the location with ID : ' + value, response.status, response.data);
				});*/

				window[nameOfFunction](value);
			};

			//scope.showContentBottom=true;
			scope.showFilterBottom = false;

			//console.log(element);

			var map = [];
			var infoWindow;
    	    var options = {
   				componentRestrictions: {country: 'be'}
			};

			//scope.displayMap = initMap;
			scope.initApp = initApp;
			function initApp(){
				if(scope.ladActive){
					scope.darkStoreId=undefined;
					scope.ladAvailable=undefined;
					initGeocomplete();
				}else{
					$q.all([initMap(),scope.getMenuDataFromAPI()]).then(function(value) {

						scope.showCategories();

						//setTimeout(function() {
						//	initGeocomplete();
						//}, 1000);
                        initGeocomplete();

						if(scope.showBottom){
							scope.toggleBottomContent();
						}
			        },function(error) {
		                console.log('Application Initialization Error', error);
		            },function(notify) {
		                console.log('notify', notify);
		            });
	            }
			}

        	// init the map
        	function initMap() {

        		var deferred = $.Deferred();
					// map = [];
            	if (map[scope.id] === void 0) {

					var centerLat = 50.846709;
					var centerLng = 4.3523343;
					var radius = 350;
					var defaultZoom = 13;

					if (scope.lgParameters.lgPosition != undefined) {
						centerLat = scope.lgParameters.lgPosition[0];
						centerLng = scope.lgParameters.lgPosition[1];
					}else{
						scope.lgParameters.lgPosition=[];
						scope.lgParameters.lgPosition[0]=centerLat;
						scope.lgParameters.lgPosition[1]=centerLng;
						scope.lgParameters.lgPosition[2]=radius;
					}
				    // map config
					var mapOptions = {
					    center: new google.maps.LatLng(centerLat, centerLng),
			    		zoom: defaultZoom,
				        mapTypeId: google.maps.MapTypeId.ROADMAP,
					    scrollwheel: false,
					    zoomControl: false,
						scaleControl: false,
						mapTypeControl: false
					};

        	    	map[scope.id] = new google.maps.Map(document.getElementById("map_"+scope.id), mapOptions);

					// Setup the click event listener - zoomIn
      				google.maps.event.addDomListener(document.getElementById('icon-plus-circle'), 'click', function() {
      					map[scope.id].setZoom(map[scope.id].getZoom() + 1);
      				});

       				// Setup the click event listener - zoomOut
       				google.maps.event.addDomListener(document.getElementById('icon-min-circle'), 'click', function() {
       					if(map[scope.id].getZoom()>9){
       						map[scope.id].setZoom(map[scope.id].getZoom() - 1);
       						if(scope.n<150){
       							scope.n=scope.n*2;
       							var rad = scope.lgParameters.lgPosition[2];
       							if(rad<radius){
       								scope.lgParameters.lgPosition[2]=rad*2;
       							}
       							scope.reloadResults(false);
       						}
       					}
       				});

       				//Set new coordinates when the user move(drag) on the map
       				google.maps.event.addListener(map[scope.id], 'dragend', function(evt){
       					scope.lgParameters.lgPosition[0]=this.getCenter().lat().toFixed(3);
       					scope.lgParameters.lgPosition[1]=this.getCenter().lng().toFixed(3);
       					scope.reloadResults(false);
       				});

					deferred.resolve();
            	}else{
            		deferred.resolve();
            	}

            	return deferred.promise();
        	};

        	function initGeocomplete(){
        		// initialize the autocomplete
    	    	angular.element('#lg-autocomplete').geocomplete({
      				country: 'be'
    			}).bind("geocode:result",function(event,result){
    				if(scope.ladActive){

						// make a second query to get a valid adress to get the zip code from
						var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'location': result.geometry.location}, function(list, status) {
                            var zip;
                            //console.log(result);
                            var fullResult = list[0];
                            for(detail of fullResult.address_components){
                                if(detail.types){
                                    if(detail.types[0]=='postal_code'){
                                        zip=detail.long_name;
                                        break;
                                    }
                                }
                            }
                            if(zip!=undefined && zip.match(/[1-9][0-9]{3}/)){
                                scope.getDarkStores(zip);
                            }
                        });
                	}else{
	    				scope.lgParameters.lgPosition[0]=result.geometry.location.lat();
	    				scope.lgParameters.lgPosition[1]=result.geometry.location.lng();
	    				scope.reloadResults(true);
	    				scope.showLocationPopup=true;
    				}
    			});
    			if(!scope.ladActive){
	    			if(scope.lgParameters.lgZipcode || scope.lgParameters.lgAddress){
						var address = usefullFn.getFormatedAddress(scope.lgParameters.lgAddress, scope.lgParameters.lgZipcode, scope.lgParameters.lgLanguage);
						if(address){
							angular.element('#lg-autocomplete').geocomplete("find", address);
						}
						else{
							scope.reloadResults(true);
						}
					}else{
						scope.reloadResults(true);
					}
				}
        	}

        	scope.fitZoomLevel = function(){
				var minMarkerCount = 5;
				var count = 0;
                for (var i=0; i<scope.locations.length; i++){
                    if(map[scope.id].getBounds().contains(new google.maps.LatLng(scope.locations[i].address.coordinates.latitude, scope.locations[i].address.coordinates.longitude))){
                        count++;
                    }
                    if(count == minMarkerCount || i == minMarkerCount-1){
                    	break;
					}
                }
                if(count < minMarkerCount){
                    var bounds = new google.maps.LatLngBounds();
                    for(var i = 0; (i < scope.locations.length) && (i < minMarkerCount-1); i++){
                        bounds.extend(new google.maps.LatLng(scope.locations[i].address.coordinates.latitude, scope.locations[i].address.coordinates.longitude));
                    }
                    map[scope.id].fitBounds(bounds);
				}
			};

        	scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
        		if(scope.slickLocations.length>0){
        			$('.slide_home_magasin').slick({
        				dots: false,
   						infinite: false,
   						slidesToShow: 4,
     					slidesToScroll: 1
      				});
        		}else{
        			$('.slide_home_magasin').slick({
  						centerMode: true,
  						dots: false,
   						infinite: false,
   					}).slick('slickAdd','<div><h3>' + "NOUS SOMMES DÉSOLÉS, IL NY A AUCUN RÉSULTAT À PROXIMITÉ DE VOTRE POSITION." + '</h3></div>');

        		}
      			$('.slide_home_magasin').css("visibility", "visible");
			});

        	scope.updateSlick = function(){
        		$('.slide_home_magasin').css("visibility", "hidden");;

				if ($('.slide_home_magasin').hasClass("slick-initialized")) {
    				$('.slide_home_magasin').slick('unslick');
				}

        		var i=0;
        		scope.slickLocations.length=0;
        		for(eachLocation of scope.locations){
        			if(i<10){
						scope.slickLocations[i]=eachLocation;
						i++;
					}
					else{
						break;
					}
        		}
        	};

			scope.showCategories = function() {
					var categoryToShow=[];
					for (eachSubcategory of scope.servicesPerInstance[scope.id]) {
						var reg = new RegExp("\\b"+ eachSubcategory.serviceID +"\\b", "g");
   						if (scope.servicesFixed.match(reg)) {
							eachSubcategory.active = true;
							eachSubcategory.show = false;
						}
						else if(scope.servicesChecked.match(reg)) {
							eachSubcategory.active = true;
							eachSubcategory.show = true;
							styleSheetFactory.addCSSRule(styleSheet, '#'+eachSubcategory.serviceID+'_'+scope.id,
								'background: #4d4538;'+
								'box-shadow: 0px 2px 2px rgba(0, 0, 0, .25) inset;'+
								'border-bottom: 1px solid #655b4c;'
							);
						}
						else if(scope.servicesVisible.match(reg)) {
							eachSubcategory.active = false;
							eachSubcategory.show = true;
						}
						else {
							eachSubcategory.active = false;
							eachSubcategory.show = false;
						}
						if(eachSubcategory.show==true){
							if(categoryToShow.indexOf(eachSubcategory.categoryList) === -1){
								categoryToShow.push(eachSubcategory.categoryList);
							}
						}
					}
					for(eachCategory of scope.categoriesPerInstance[scope.id]){
						eachCategory.show=false;
						if(eachCategory.catID=="CAT_HOURS"){
							eachCategory.show=true;
						}
						else{
							for (eachItem of categoryToShow) {
								if(eachCategory.catID == eachItem){
									eachCategory.show=true;
									break;
								}
							}
						}
					}
					if(scope.openNowSubcategory){
						styleSheetFactory.addCSSRule(styleSheet, '#openNow_'+scope.id,
								'background: #4d4538;'+
								'box-shadow: 0px 2px 2px rgba(0, 0, 0, .25) inset;'+
								'border-bottom: 1px solid #655b4c;'
							);
					}
					if(scope.openSundaySubcategory){
						styleSheetFactory.addCSSRule(styleSheet, '#openSunday_'+scope.id,
								'background: #4d4538;'+
								'box-shadow: 0px 2px 2px rgba(0, 0, 0, .25) inset;'+
								'border-bottom: 1px solid #655b4c;'
							);
					}

					var i = 2;
					for (eachCategory of scope.categoriesPerInstance[scope.id]) {
						if(eachCategory.show==true || eachCategory.catID=="CAT_HOURS"){
							if (i%2 == 0) {
								eachCategory.style = 'soft';
							}else {
								eachCategory.style = 'dark';
							}
							i++;
						}
					}
			};

			scope.reloadResults = function(adaptZoom) {
				scope.getResults(map[scope.id],adaptZoom);
			};

			scope.triggerGeo = function() {
				var firstResult = angular.element(".pac-container .pac-item:first").text(); // or angular.element('#lg-autocomplete').geocomplete("selectFirstResult");
				if(firstResult != undefined && firstResult != ''){
					document.getElementById("lg-autocomplete").value = firstResult;
					angular.element('#lg-autocomplete').trigger("geocode");
				}
			};

			scope.disableService = function(subcategoryId, id){
				if(scope.servicesFixed.indexOf(subcategoryId) == -1){
					scope.toggleSubcategorySelection(subcategoryId,id);
					scope.reloadResults(true);
				}
			};

			scope.geolocate = function(){
				scope.loading=true;
				if (navigator.geolocation) {
					document.getElementById("lg-autocomplete").value = "";
		          	navigator.geolocation.getCurrentPosition(function(position) {
		            if(position.coords.latitude && position.coords.longitude){
		            	scope.lgParameters.lgPosition[0]=position.coords.latitude;
    					scope.lgParameters.lgPosition[1]=position.coords.longitude;
    					scope.reloadResults(true);
    					scope.showLocationPopup=true;
    				}
		          }, function() {
		            console.log("The Geolocation service failed.");
		            scope.loading=false;
		          });
		        } else {
		          // Browser doesn't support Geolocation
		          console.log("This browser doesn\'t support geolocation.");
		          scope.loading=false;
		        }
			}

       		// show the map and place some markers
        	//initMap();
			scope.toggleFilterContent = function(idOfPicker) {
				if (scope.showFilterBottom == true) {
					scope.showFilterBottom = false;
            		styleSheetFactory.removeCSSRule(styleSheet, '#filter_' + idOfPicker);
            		styleSheetFactory.addCSSRule(styleSheet, '#filter_' + idOfPicker,
							'display: none;');
            		document.getElementById('up-down-arrow-filter').className = 'icon-arrow-down';
				} else {
					scope.showFilterBottom = true;
            		styleSheetFactory.removeCSSRule(styleSheet, '#filter_' + idOfPicker);
            		styleSheetFactory.addCSSRule(styleSheet, '#filter_' + idOfPicker,
							'display: inline-flex;');
            		document.getElementById('up-down-arrow-filter').className = 'icon-arrow-up';
				}
					//console.log(scope.showContentBottom);
			};


			scope.toggleBottomContent = function() {
				if (scope.showContentBottom == false) {
					scope.showContentBottom = true;
        			styleSheetFactory.removeCSSRule(styleSheet, '.hide_all_content');
        			styleSheetFactory.addCSSRule(styleSheet, '.hide_all_content',
						'display: none;');
        			document.getElementById('up-down-arrow').className = 'icon-arrow-up';
				} else {
					scope.showContentBottom = false;
        			styleSheetFactory.removeCSSRule(styleSheet, '.hide_all_content');
        			styleSheetFactory.addCSSRule(styleSheet, '.hide_all_content',
						'display: inline;'
					);
        			document.getElementById('up-down-arrow').className = 'icon-arrow-down';
        			if(scope.slickLocations != undefined && scope.slickLocations.length > 0){
        				$('.slide_home_magasin').slick('resize');
        			}
				}
					//console.log(scope.showContentBottom);
			};

			var styleSheet = styleSheetFactory.getStyleSheet();

			scope.toggleSubcategoriesVisibility = function(categoryId, id) {

					styleSheetFactory.removeCSSRule(styleSheet, '#CAT_HOURS_'+id);
					for (eachCategory of scope.categoriesPerInstance[scope.id]) {
						//console.log(eachCategory);
						styleSheetFactory.removeCSSRule(styleSheet, '#'+eachCategory.catID+'_'+id);
					}

					if (window.currentCategory[scope.id] == categoryId) {
						window.currentCategory[scope.id] = '';
					}
					else {
						window.currentCategory[scope.id] = categoryId;
						styleSheetFactory.addCSSRule(styleSheet, '#'+categoryId+'_'+id,
							'display: inline-block;'
						);
					}
			};

			scope.toggleSubcategorySelection = function(subcategoryId, id) {

					if (subcategoryId == 'openNow') {
						if (scope.openNowSubcategory == true) {
							styleSheetFactory.removeCSSRule(styleSheet, '#openNow_'+id);
						} else {
							styleSheetFactory.addCSSRule(styleSheet, '#openNow_'+id,
								'background: #4d4538;'+
								'box-shadow: 0px 2px 2px rgba(0, 0, 0, .25) inset;'+
								'border-bottom: 1px solid #655b4c;'
							);
						}
						scope.openNowSubcategory = !scope.openNowSubcategory;
					} else if (subcategoryId == 'openSunday') {
						if (scope.openSundaySubcategory == true) {
							styleSheetFactory.removeCSSRule(styleSheet, '#openSunday_'+id);
						} else {
							styleSheetFactory.addCSSRule(styleSheet, '#openSunday_'+id,
								'background: #4d4538;'+
								'box-shadow: 0px 2px 2px rgba(0, 0, 0, .25) inset;'+
								'border-bottom: 1px solid #655b4c;'
							);
						}
						scope.openSundaySubcategory = !scope.openSundaySubcategory;
					}	else if (subcategoryId == 'openOn') {
						scope.lgParameters.lgOpenDate=null;
						scope.$broadcast('clearDate');
					}

					for (eachSubcategory of scope.servicesPerInstance[scope.id]) {
						if (eachSubcategory.serviceID == subcategoryId) {
							if (eachSubcategory.active == true) {
								if(scope.servicesFixed.indexOf(subcategoryId) == -1){
									eachSubcategory.active = false;
									styleSheetFactory.removeCSSRule(styleSheet, '#'+eachSubcategory.serviceID+'_'+id);
								}
							}
							else {
								eachSubcategory.active = true;
								styleSheetFactory.addCSSRule(styleSheet, '#'+eachSubcategory.serviceID+'_'+id,
									'background: #4d4538;'+
									'box-shadow: 0px 2px 2px rgba(0, 0, 0, .25) inset;'+
									'border-bottom: 1px solid #655b4c;'
								);
							}
							break;
						}
					}
			};
		}
	};
}]);

lgStorePicker.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        require:'^lgStorePicker',
        link: function (scope, element, attr, lgStorePickerCtrl) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

lgStorePicker.directive('datepicker', function() {
        return {
            restrict: 'AE',
            require: ['^lgStorePicker', 'ngModel'],
            link : function (scope, element, attrs, lgStorePickerCtrl) {
                   	var datepicker = element.datepicker({
                        language: scope.language,
                        dateFormat:'yyyy-mm-dd',
                        position: "bottom right",
                        onSelect: function(formattedDate, date, inst){
                            //var selected = element.val();
                            //console.log(selected);
                            scope.lgParameters.lgOpenDate=formattedDate;
                            scope.reloadResults(true);
                            inst.hide();
                        }
                   	}).data('datepicker');;
                   	scope.$on('clearDate', function () {
            			datepicker.clear();
          			})

            }
        }
});


/***********************************************************************
Style Sheet Factory
Author: Brenton Klik
Prerequisites: AngularJS
Description:
This factory provides a series of methods to make management of CSS
styles in javascript easier. Directives may take advantage of these
to include thier CSS as part of their code, rather than an external
style sheet.
/**********************************************************************/
angular.module('style-sheet-factory', [])

.factory('styleSheetFactory', ['$log', function($log) {
	/************************************************************************
	Local Variables
	************************************************************************/
	// Array of selectors modified by the browser.
	var _modifiedSelectors = [];

	var _insertCSSRule = function(sheet, selector, rules, index) {
		index = index || 1;

		try {
			sheet.insertRule(selector + "{" + rules + "}", index);

			// Parse browser's selector
			var newSelector = sheet.cssRules[index].cssText.split(' {')[0];

			// If browser modified the selector, store it.
			if(selector !== newSelector) {
				var selectorObj = {
					'old': selector,
					'new': newSelector
				};

				_modifiedSelectors.push(selectorObj);
			}

			return sheet.cssRules[1].cssText;
		} catch(e) {
			$log.error('Failed to add rule: ' + selector);
		}
	};

	return {
		// Finds and returns the browsers's main style sheet.
		getStyleSheet: function() {
			for(var i=0; i<document.styleSheets.length; i++) {
				if(
					document.styleSheets[i].media.mediaText === '' ||
					document.styleSheets[i].media.mediaText === 'all' ||
					document.styleSheets[i].media.mediaText === 'screen'
				) {
					return document.styleSheets[i];
				}
			}

			return null;
		},

		// Gets the prefix related to the user's browser type. Used in
		// CSS for non-standardized properties.
		getPrefix: function() {
			var prefixes = ['Webkit', 'Moz', 'ms', 'O', 'Khtml'];
			var len = prefixes.length;

			for(var i=0; i<len; i++) {
				if(document.body.style[ prefixes[i] + 'AnimationName' ] !== undefined) {
					return '-'+prefixes[i].toLowerCase()+'-';
				}
			}
			return '';
		},

		// Returns whether a rule of that selector exists in the stylesheet.
		hasCSSRule: function(sheet, selector) {
			var rules = sheet.cssRules;
			var len = _modifiedSelectors.length

			// Check for a modified selector.
			for(var m=0; m<len; m++) {
				if(selector === _modifiedSelectors[m].old) {
					selector = _modifiedSelectors[m].new;
				}
			}

			len = rules.length;

			for(var i=0; i<len; i++) {
				if(rules[i].selectorText === selector) {
					return true;
				}
			}

			return false;
		},

		// Returns whether a keyframe of that name exists in the stylesheet.
		hasCSSKeyframes: function(sheet, name) {
			var rules = sheet.cssRules;
			var len = rules.length;

			for(var i=0; i<len; i++) {
				if(rules[i].name === name) {
					return true;
				}
			}

			return false;
		},

		// If no selector of that rule exists, adds the new rule to the stylesheet.
		addCSSRule: function(sheet, selector, rules, index) {
			if(!this.hasCSSRule(sheet, selector)) {
				return _insertCSSRule(sheet, selector, rules, index);
			}
		},

		// Removes a rule of the given selector from the stylesheet.
		removeCSSRule: function(sheet, selector) {
			var rules = sheet.cssRules;
			var len = _modifiedSelectors.length;

			// Check for a modified selector and remove it.
			for(var m=0; m<len; m++) {
				if(selector === _modifiedSelectors[m].old) {
					selector = _modifiedSelectors[m].new;

					_modifiedSelectors.splice(m, 1);
				}
			}

			len = rules.length;

			for(var i=0; i<len; i++) {
				if(rules[i].selectorText === selector) {
					sheet.deleteRule(i);
					return true;
				}
			}

			return false;
		},

		// Removes a keyframe of the given name from the stylesheet.
		removeCSSKeyframes: function(sheet, name) {
			var rules = sheet.cssRules;
			var len = rules.length;

			for(var i=0; i<len; i++) {
				if(rules[i].name === name) {
					sheet.deleteRule(i);
					return true;
				}
			}

			return false;
		},

		// Adds a keyframes animation to the stylesheet with te appropriate prefixing.
		addCSSKeyframes: function(sheet, name, rules, index) {
			if(!this.hasCSSKeyframes(sheet, name)) {
				return _insertCSSRule(sheet, '@'+this.getPrefix()+'keyframes '+name, rules, index);
			}
		}
	}
}]);


/*
* angular-css-injector v1.0.4
* Written by Gabriel Delépine
* Special thanks to (github users) : @kleiinnn
* License: MIT
* https://github.com/Yappli/angular-css-injector/
*/
angular.module('angular.css.injector', [])
.provider('cssInjector', ['$interpolateProvider', function($interpolateProvider) {
	var singlePageMode = false;

	function CssInjector($compile, $rootScope, $rootElement){
        // Variables
        var head = angular.element(document.getElementsByTagName('head')[0]),
            scope;

        // Capture the event `locationChangeStart` when the url change. If singlePageMode===TRUE, call the function `removeAll`
        $rootScope.$on('$locationChangeStart', function()
        {
            if(singlePageMode === true)
                removeAll();
        });

        // Always called by the functions `addStylesheet` and `removeAll` to initialize the variable `scope`
        var _initScope = function()
        {
            if(scope === undefined)
            {
                scope = $rootElement.scope();
            }
        };

        // Used to add a CSS files in the head tag of the page
        var addStylesheet = function(href)
        {
            _initScope();

            if(scope.injectedStylesheets === undefined)
            {
                scope.injectedStylesheets = [];
                head.append($compile("<link data-ng-repeat='stylesheet in injectedStylesheets' data-ng-href='" + $interpolateProvider.startSymbol() + "stylesheet.href" + $interpolateProvider.endSymbol() + "' rel='stylesheet' />")(scope)); // Found here : http://stackoverflow.com/a/11913182/1662766
            }
            else
            {
                for(var i in scope.injectedStylesheets)
                {
                    if(scope.injectedStylesheets[i].href == href) // An url can't be added more than once. I use a loop FOR, not the function indexOf to make the code IE < 9 compatible
                        return;
                }
            }

            scope.injectedStylesheets.push({href: href});
        };

		var remove = function(href){
			_initScope();

			if(scope.injectedStylesheets){
				for(var i = 0; i < scope.injectedStylesheets.length; i++){
					if(scope.injectedStylesheets[i].href === href){
						scope.injectedStylesheets.splice(i, 1);
						return;
					}
				}
			}
		};

        // Used to remove all of the CSS files added with the function `addStylesheet`
        var removeAll = function()
        {
            _initScope();

            if(scope.injectedStylesheets !== undefined)
                scope.injectedStylesheets = []; // Make it empty
        };

        return {
            add: addStylesheet,
			remove: remove,
            removeAll: removeAll
        };
	}

	this.$get = ['$compile', '$rootScope', '$rootElement', function($compile, $rootScope, $rootElement){
		return new CssInjector($compile, $rootScope, $rootElement);
	}];

	this.setSinglePageMode = function(mode){
		singlePageMode = mode;
		return this;
	}
}]);



/***********************************************************************
Popup Directive
Author: Brenton Klik
Prerequisites:
 - AngularJS
 - styleSheetFactory (https://github.com/bklik/styleSheetFactory)
Description:
Create a popup positioned under the given element.
/**********************************************************************/
angular.module('popup-directive', ['style-sheet-factory'])

.directive('popup', ['styleSheetFactory', function(styleSheetFactory) {
	return {
		scope: {
			api: '=',
			hideCallback: '&'
		},
		restrict: 'E',
		link: function($scope, $element, $attrs) {
			/************************************************************************
			API
			************************************************************************/
			$scope.api = {
				show: function(event) {
					show(event);
				},

				hide: function() {
					hide();
				},

				toggle: function(event) {
					if ($scope.chooseButtonVisibility == "show") {
						hide();
						$scope.chooseButtonVisibility = "hide";
					}
					else {
						show(event);
						$scope.chooseButtonVisibility = "show";
					}
				},
			};

			/************************************************************************
			Variables
			************************************************************************/
			// The document's stylesheet.
			var styleSheet = styleSheetFactory.getStyleSheet();

			// The prefix used by the browser for non-standard properties.
			var prefix = styleSheetFactory.getPrefix();

			// Target element the popup should appear next to.
			var target = null;

			// Used to track whether or not a move happened during a touch.
			var touchMove = false;

			// Used by event listeners to prevent the popup from closing.
			var preventClose = function(event) {
				event.stopPropagation();
			}

			/************************************************************************
			Methods
			************************************************************************/
			// Closes the popup if a touch happens and no touchmove event fired.
			var touchHandler = function(event) {
				if(event.type === "touchmove") {
					touchMove = true;
				} else if(event.type === 'touchend') {
					if(!touchMove) {
						hide();
					}
					touchMove = false;
				}
			};

			// Display the popup
			var show = function(event) {
				if(typeof event !== 'undefined') {
					if(typeof event.target !== 'undefined') {
						target = event.target;
					} else {
						target = event;
					}

					$element.addClass('show');
					position(target);

					target.addEventListener('mousedown', preventClose);
					//target.addEventListener('keydown', hide);
					//window.addEventListener('mousedown', hide);
					window.addEventListener('touchmove', touchHandler);
					window.addEventListener('touchend', touchHandler, false);
				} else {
					console.error('Popup Directive method "show" requires a target element.');
				}
			};

			// Hide the popup
			var hide = function(event) {
				if(typeof $scope.hideCallback == 'function') {
					$scope.hideCallback();
				};

				$element.removeClass('show');
				target.removeEventListener('mousedown', preventClose);
				//target.removeEventListener('keydown', hide);
				//window.removeEventListener('mousedown', hide);
				window.removeEventListener('touchmove', touchHandler);
				window.removeEventListener('touchend', touchHandler);

				target = null;
				$element.removeClass('adjust-arrow');
				styleSheetFactory.removeCSSRule(styleSheet, 'popup.adjust-arrow::after');
			}

			// Position the popup under the element that triggered the event.
			var position = function(target) {
				var targetRect = target.getBoundingClientRect();
				var popupRect = $element[0].getBoundingClientRect();
				var bodyRect = document.body.getBoundingClientRect();
				var parentRect = target.parentNode.getBoundingClientRect();

				// var top = (targetRect.top - parentRect.top) + targetRect.height + 16;
				// var left = (targetRect.left - parentRect.left);

				var top = 0;
				var left = 0;

				// Make sure the popup isn't off the edge of the page.
				if(targetRect.left + popupRect.width > bodyRect.width) {
					var adjustment = ((targetRect.left + popupRect.width) - bodyRect.width);
					left = left - adjustment;

					styleSheetFactory.addCSSRule(styleSheet, 'popup.adjust-arrow::after',
						'left: '+(10 + adjustment)+'px;'
					);
					$element.addClass('adjust-arrow');
				}
				if (left < 0) {
					left = 0;
				}
				// $element.attr('style',
				// 	'top: '+top+'px;' +
				// 	'left: '+left+'px;' +
				// 	'position: absolute;'
				// );
			};

			/************************************************************************
			Init
			************************************************************************/
			// Prevent close if any touchs/clicks happen inside the popup
			$element.bind('mousedown', preventClose);
			$element[0].addEventListener('touchend', preventClose, true);

			/************************************************************************
			Styles
			************************************************************************/

			// Add this directive's styles to the document's stylesheet.
			styleSheetFactory.addCSSRule(styleSheet, 'popup',
				'background: white;' +
				'border-radius: 2px;' +
				//'box-shadow: 0 1px 4px rgba(0,0,0,0.25);' +
				'display: none;' +
				'min-height: 32px;' +
				'min-width: 32px;' +
				'overflow: visible;' +
				'padding: 8px;' +
				'position: static;' +
				'transform: translateZ(0);' +
				prefix+'transform: translateZ(0);' +
				'z-index: 1;' +
				'width: 100%;' +
				'width: -moz-available;' +
				'width: -webkit-fill-available;' +
        'height: ' + (window.innerHeight - 60) + 'px;'
			);

			styleSheetFactory.addCSSRule(styleSheet, 'popup::after',
				'background-color: white;' +
				'border-left: 1px solid rgba(0,0,0,0.08);' +
				'border-top: 1px solid rgba(0,0,0,0.08);' +
				'box-sizing: border-box;' +
				'content: \'\';' +
				'display: block;' +
				'' +
				'position: absolute;' +
				'top: -6px;' +
				'left: 10px;' +
				'height: 10px;' +
				'width: 10px;' +
				'' +
				prefix+'transform: rotate(45deg);' +
				'transform: rotate(45deg);'
			);

			styleSheetFactory.addCSSRule(styleSheet, 'popup.show',
				'display: inline-block;' +
				'' +
				'-webkit-animation: popup-slidein 250ms;' +
				'-moz-animation: popup-slidein 250ms;' +
				'animation: popup-slidein 250ms;'
			);

			styleSheetFactory.addCSSKeyframes(styleSheet, 'popup-slidein',
				'from {' +
					'opacity: 0;' +
					prefix+'transform: translateY(16px);' +
					'transform: translateY(16px);' +
				'}' +
				'to {' +
					'opacity: 1;' +
					prefix+'transform: translateY(0);' +
					'transform: translateY(0);' +
				'}'
			);
		}
	}
}]);
