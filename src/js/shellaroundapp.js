import Helper from './helper';

export default class ShellAroundApp {
  constructor() {
    this.eventBus = null;
    this.initialized = false;
    this.state = "";
  }

  // Initiate the module and its
  init () {
    document.addEventListener('crf_module_init',  (result) => {
      var register = result.detail;

      var moduleData = {
        name: 'AddToBasketWebmodule', // Set the name of the module to be registered
        version: '0.0.1', // Set the registration version of the module, all versions of all modules must match
        eventTypes: [
          'core/state/running',
          'core/api/onToken',
          'core/api/request',

          'authentication/isAuthenticated',
          'authentication/isAuthenticatedDone',

          'store/singleProductByBiggestStore',
          'store/singleProductByBiggestStoreDone',
          'store/getDriveStore',
          'store/getDefaultTimeslot',
          'store/getDefaultTimeslotDone',
          'analytics/trackView',
          'store/searchIngredient'
        ],
        isInitialModule: false, // Only 1 module can be the Initial Module
        isAuthenticationModule: false // Only 1 module can be the authenticationModule
      };

      // Call registration function with the moduleData variable to register it
      register(moduleData,  (eventbus) => {
        this.eventBus = eventbus;
        this.eventBus.registerHandler("core/state/running", (eventType) => {
          this.registerEvents();
        });
      });
    });
  }

  // Method to register all initial events
  registerEvents () {
    this.eventBus.registerHandler("core/closeApp", (eventType, basket) => {
      if (document.body.classList.contains('app-open')) {
        document.body.classList.remove('app-open');
      }
    });

    this.eventBus.registerHandler("basket/getDone", (eventType, basket) => {
      document.body.classList.add("basket-known");

      if (basket) {
        document.querySelector(".items-in-basket").innerHTML = basket.productCount;
      }

      if (!this.initialized) {
        this.initialized = true;
        if(localStorage.getItem('carrefour.lastEvent')) {
          var lastEvent = JSON.parse(localStorage.getItem('carrefour.lastEvent'))
          this.detectEvent(lastEvent.action, lastEvent.params);
        }
      }
    });

    this.eventBus.registerHandler("store/getDriveStore", (eventType, store) => {
      if(store) {
        Helper.showToaster("Je voegde drive store toe.")
      }

    });

    this.eventBus.registerHandler("authentication/isAuthenticatedDone", (eventType, isAuthenticated) => {
      if (!isAuthenticated) {
        Helper.showToaster("Je bent niet ingelogd. Klik eerst op het user icoon.");
      }
      document.body.classList.add("basket-known");

    });

    this.eventBus.registerHandler("core/stateTransition", (eventType, data) => {
      this.state = data;
    });

    this.eventBus.throwEvent("authentication/isAuthenticated");
  }

  detectEvent (action, ...params) {

    // =========================== //
    // User clicked on show basket //
    // =========================== //
    if (action && action == "show-basket") {
      if (!document.body.classList.contains('app-open')) {
        document.body.classList.add('app-open');

        if(!Helper.checkDriveStore()) {
          if(this.state !== "FavStorePage") {
            this.searchStore();
          }
        } else {
          if(this.state !== "BasketPage") {
            this.showBasket();
          }
        }

      } else {
        document.body.classList.remove('app-open');
      }
    }

    // ============================ //
    // User clicked on show product //
    // ============================ //
    if (action && action == "show-productdetail") {
      if (!document.body.classList.contains('app-open')) {
        document.body.classList.add('app-open');
      }

      if(!Helper.checkDriveStore()) {
        if(this.state !== "FavStorePage") {
          this.searchStore();
        }
      } else {
        this.showProductDetail(params[0]);
      }
    }

    // ===================================== //
    // User clicked on add product to basket //
    // ===================================== //
    if (action && action == "add-product-to-basket") {
      if(!Helper.checkDriveStore()) {
        if (!document.body.classList.contains('app-open')) {
          document.body.classList.add('app-open');
        }
        if(this.state !== "FavStorePage") {
          this.searchStore();
        }
      } else {
        if (document.body.classList.contains('app-open')) {
          document.body.classList.remove('app-open');
        }
        this.addProductToBasket(params[0]);
      }
    }

    // ================================= //
    // User clicked on search ingredient //
    // ================================= //
    if (action && action == "search-ingredient") {
      if (!document.body.classList.contains('app-open')) {
        document.body.classList.add('app-open');
      }


      if(!Helper.checkDriveStore()) {
        if(this.state !== "FavStorePage") {
          this.searchStore();
        }
      } else {
        this.searchIngredient(params[0]);
      }
    }

    // ============================ //
    // User clicked on select store //
    // ============================ //
    if (action && action == "show-store") {
      if (!document.body.classList.contains('app-open')) {
        document.body.classList.add('app-open');
      }

      if(this.state !== "FavStorePage") {
        this.searchStore();
      }
    }
  }

  showBasket () {
    console.log("SHOW BASKET");
    // Throw navigation event to basketPage
    this.eventBus.throwEvent('app/navigate', {key: 'drive/basketPage'});
  }

  searchStore () {
    console.log("SEARCH STORE");
    // Throw navigation event to favstorePage
    this.eventBus.throwEvent('app/navigate', {key: 'store/favStore'});
  }

  showProductDetail (productId) {
    console.log("SHOW PRODUCT DETAIL:", productId);
    const singleProductCallback = (eventType, product) => {
      if (product) {
        this.eventBus.throwEvent('app/navigate', {
            key: 'drive/productInfoPage',
            params: {
              product: product
            }
        });
      }
      this.eventBus.unregisterHandlerByFunction(singleProductCallback);
    }

    const timeslotCallback = (eventType, time) => {
      this.eventBus.throwEvent("store/singleProductByBiggestStore", {product: productId});
      this.eventBus.unregisterHandlerByFunction(timeslotCallback);
    }

    this.eventBus.registerHandler("store/singleProductByBiggestStoreDone", singleProductCallback);
    this.eventBus.registerHandler("store/getDefaultTimeslotDone", timeslotCallback);

    // Throw event to get the default timeslot, tis is mandatory to get a product
    this.eventBus.throwEvent("store/getDefaultTimeslot");
  }

  addProductToBasket (productId) {
    console.log("ADD PRODUCT TO BASKET:", productId);

    const cbUpdate = (eventType, basket) => {
      var myProduct = null;
      basket.products.forEach((product) => {
        if(product.ref == productId) {
          myProduct = product;
          product.addAmount(product.stepToAddOrRemove);
          Helper.showToaster("Je voegde " + product.shortDesc + "(" + product.price.stdPrice + "€) toe.")
          this.eventBus.throwEvent("basket/update", product);
        }
      });

      if(myProduct) {
        if(this.state !== "BasketPage"){
          this.eventBus.throwEvent('app/navigate', {key: 'drive/basketPage'});
        }
      } else {
        this.eventBus.registerHandler("store/singleProductByBiggestStoreDone", singleProductCallback);
        this.eventBus.registerHandler("store/getDefaultTimeslotDone", timeslotCallback);

        // Throw event to get the default timeslot, tis is mandatory to get a product
        this.eventBus.throwEvent("store/getDefaultTimeslot");
      }

      this.eventBus.unregisterHandlerByFunction(cbUpdate);
    }

    const cbUpdateSingle = (eventType, basket) => {
      if(this.state !== "BasketPage"){
        this.eventBus.throwEvent('app/navigate', {key: 'drive/basketPage'});
      }
      this.eventBus.unregisterHandlerByFunction(cbUpdateSingle);
    }

    const singleProductCallback = (eventType, product) => {
      if (product) {
        product.addAmount(product.stepToAddOrRemove);
        Helper.showToaster("Je voegde " + product.shortDesc+ " (" + product.price.stdPrice + "€) toe.")
        this.eventBus.registerHandler("basket/getDone", cbUpdateSingle);
        this.eventBus.throwEvent("basket/update", product);
      }
      this.eventBus.unregisterHandlerByFunction(singleProductCallback);
    }

    const timeslotCallback = (eventType, time) => {
      this.eventBus.throwEvent("store/singleProductByBiggestStore", {product: productId});
      this.eventBus.unregisterHandlerByFunction(timeslotCallback);
    }

    this.eventBus.registerHandler("basket/getDone", cbUpdate);
    this.eventBus.throwEvent("basket/get");
  }

  searchIngredient (ingredient) {
    console.log("SEARCH INGREDIENT", ingredient);
    console.log(this.state);
    // Check if you are on the categoriespage first...go to it when you are not on it to see the results.
    if(this.state !== "CategoriesPage") {
      this.eventBus.throwEvent('app/navigate', {key: 'drive'});
    }

    // throw custom event
    this.eventBus.throwEvent("store/searchIngredient", ingredient);
  }
}

