export default class Helper {
  static showToaster (text) {
    var toaster = document.querySelector('.toaster');
    var toasterMessage = document.querySelector('.toaster__message');

    toaster.classList.add("toaster--shown");
    toasterMessage.innerHTML = text;

    let timer = setTimeout(() => {
      toaster.classList.remove("toaster--shown");
      clearTimeout(timer);
    }, 5000);
  };

  static checkDriveStore () {
    if (localStorage.getItem("store.drive") === null) { return false; }
    return true;
  }

  static loadJS (url, implementationCode, location){
    let scriptTag = document.createElement('script');
    scriptTag.src = url;
    scriptTag.onload = implementationCode;
    scriptTag.onreadystatechange = implementationCode;
    location.appendChild(scriptTag);
  };
}
