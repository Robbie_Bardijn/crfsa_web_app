// =========================== //
// Imports //
// =========================== //
import ShellAroundApp from './shellaroundapp';
import Helper from './helper';

// All click events on .link and .button
function clickButton(e) {

  // console.log(typeof e.target);
  localStorage.removeItem('carrefour.lastEvent');

  if (shell.initialized) {
    e.preventDefault();
    shell.detectEvent(e.currentTarget.getAttribute('data-action'), e.currentTarget.getAttribute('data-action-params'));
  } else {
    var eventData = {action: e.currentTarget.getAttribute('data-action'), params: e.currentTarget.getAttribute('data-action-params'), fallback: e.currentTarget.getAttribute('data-action-fallback')}
    localStorage.setItem('carrefour.lastEvent', JSON.stringify(eventData));

    if(!e.currentTarget.hasAttribute('href')) {
      window.location.href = e.currentTarget.getAttribute('data-action-fallback');
    }
  }
}

// Attach eventlisteners
var buttons = document.querySelectorAll('.button, .link');
buttons.forEach(button => {
  button.addEventListener("click", clickButton, true);
})

// Make an instance of shellaroundapp
let shell = new ShellAroundApp();
shell.init();
Helper.loadJS('build/main.js', function(){}, document.body);

// add classes to body to make the layout look ok
document.body.classList.add("not-logged-in");
document.body.classList.add("no-store");
