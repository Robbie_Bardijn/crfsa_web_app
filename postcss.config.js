module.exports = {
  plugins: [
    require('postcss-remove-rules')({rulesToRemove: {'body': '*', 'html' : '*'}}),
    require('postcss-wrap')({selector: '.app'})
  ]
}
